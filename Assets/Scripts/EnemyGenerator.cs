﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGenerator : MonoBehaviour
{
    public GameObject EnemyPrefab;
    void Start()
    {
        InvokeRepeating("Spawn", 2f, 0.5f);
    }

    void Spawn ()
    {
        Vector3 spawnPosition = new Vector3(
            Random.Range(-1.75f, 1.75f),
            transform.position.y,
            transform.position.z);
        Instantiate(EnemyPrefab, spawnPosition, transform.rotation);
    }

    void Update()
    {
        
    }
}
