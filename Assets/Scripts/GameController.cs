﻿using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public Text ScoreText;
    private int Score = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void AddScore(int addValue)
    {
        Score += addValue;
        ScoreText.text = Score.ToString();
    }
}
