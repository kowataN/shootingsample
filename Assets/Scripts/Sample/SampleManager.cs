﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SampleManager : MonoBehaviour
{
    private bool _AddEnemy { get; set; }
    private bool _SetTarget { get; set; }

    private GameObject _Enemy;
    public GameObject Player;
    public GameObject EnemyPrefab;

    public Transform EnemyPosition;

    public Button ButtonReset;
    public Button ButtonAddEnemy;
    public Button ButtonSetPlayer;

    public Text TextEnemyPos;
    public Text TextEnemyRot;
    public Text TextPlayerPos;

    void Start() {
        _AddEnemy = false;
        ButtonReset.onClick.AddListener(OnReset);
        ButtonAddEnemy.onClick.AddListener(OnAddEnemy);
        ButtonSetPlayer.onClick.AddListener(OnSetPlayer);
    }

    private void Update() {
        TextPlayerPos.text = string.Format("ＰＯＳ　X：{0:0.##}　　Y：{1:0.##}", Player.transform.position.x, Player.transform.position.y);
        if (_AddEnemy) {
            TextEnemyPos.text = string.Format("ＰＯＳ　X：{0:0.##}　　Y：{1:0.##}", _Enemy.transform.position.x, _Enemy.transform.position.y);
            TextEnemyRot.text = string.Format("ＲＯＴ　{0:#.#}", _Enemy.transform.rotation.z);
        }
    }

    private void OnAddEnemy() {
        if (_AddEnemy == false) {
            _Enemy = Instantiate(EnemyPrefab, EnemyPosition.position, EnemyPosition.rotation, EnemyPosition);
            _AddEnemy = true;

            ButtonSetPlayer.interactable = _AddEnemy;
            ButtonAddEnemy.interactable = false;
        }
    }

    private void OnSetPlayer() {
        if (_SetTarget) {
            _SetTarget = true;
        }
    }

    private void OnReset() {
        if (_AddEnemy) {
            Destroy(_Enemy.gameObject);
            _AddEnemy = false;
            ButtonSetPlayer.interactable = _AddEnemy;
            TextEnemyPos.text = "ＰＯＳ";
            TextEnemyRot.text = "ＲＯＴ";
             ButtonAddEnemy.interactable = true;
        }
    }
}
