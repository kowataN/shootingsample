﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    private GameObject _Target;

    public GameObject BulletPrefab;
    public Transform BulletPoint;

    public bool IsAttack { get; set; }

    public enum AttackType { Attack1, Attack2 }

    void Start()
    {
        _Target = GameObject.Find("Player");
        if (_Target == null) {
            return;
        }
    }

    public void SetEnemy(AttackType type)
    {
        switch (type)
        {
            case AttackType.Attack1:
                InvokeRepeating("Attack1", 1f, 2f);
                break;

            case AttackType.Attack2:
                break;

            default:
                break;
        }
    }

    private void Attack1() {
        if (_Target == null) {
            return;
        }
        if (IsAttack == false) {
            return;
        }

        Vector2 myPos = transform.position;
        Vector2 targetPos = _Target.transform.position;
        GameObject bullet = Instantiate(BulletPrefab, BulletPoint.position, BulletPoint.rotation);
        EnemyBullet enemyBullet = bullet.GetComponent<EnemyBullet>();
        if (enemyBullet == null) {
            return;
        }

        Vector2 basePos = targetPos - myPos;
        float rad = Mathf.Atan2(basePos.y, basePos.x);
        enemyBullet.AddPos = new Vector2(
            Mathf.Cos(rad),
            Mathf.Sin(rad))
        ;
    }
}
