﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    /// <summary>
    /// 移動速度
    /// </summary>
    public float MoveSpeed = 2f;

    void Update()
    {
        Move();
    }

    private void Move() 
    {
        float x = Input.GetAxisRaw("Horizontal");
        float y = Input.GetAxisRaw("Vertical");
        Vector3 nextPos = transform.position + new Vector3(x, y, 0) * Time.deltaTime * MoveSpeed;
        // 移動範囲クリッピング
        nextPos = new Vector3(
            Mathf.Clamp(nextPos.x, -1.75f, 1.75f),
            Mathf.Clamp(nextPos.y, -2.3f, 2.3f),
            nextPos.z
        );
        transform.position = nextPos;
    }
}
