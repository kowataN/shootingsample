﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : MonoBehaviour
{
    public Vector2 AddPos { get; set; }

    void Update()
    {
        transform.position += new Vector3(AddPos.x, AddPos.y, 0) * Time.deltaTime;

        if (transform.position.y < -3
        || transform.position.x < -3
        || transform.position.x > 3
        || transform.position.y > 3)
        {
            Destroy(gameObject);
        }

    }
}
