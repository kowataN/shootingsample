﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShip : MonoBehaviour
{
    public GameObject Explosion;
    public Transform BulletPoint;
    public GameObject BulletPrefab;

    // Update is called once per frame
    void Update()
    {
        Move();

        if (Input.GetKeyDown(KeyCode.Space)) {
            Instantiate(BulletPrefab, BulletPoint.position, BulletPoint.rotation);
        }
    }

    private void Move() 
    {
        float x = Input.GetAxisRaw("Horizontal");
        float y = Input.GetAxisRaw("Vertical");
        Vector3 nextPos = transform.position + new Vector3(x, y, 0) * Time.deltaTime * 4f;
        nextPos = new Vector3(
            Mathf.Clamp(nextPos.x, -1.75f, 1.75f),
            Mathf.Clamp(nextPos.y, -2.3f, 2.3f),
            nextPos.z
        );
        transform.position = nextPos;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log(collision.transform.ToString());
        if (collision.tag.ToString() == "EnemyBullet"
        || collision.tag.ToString() == "EnemyShip") {
            Instantiate(Explosion, transform.position, transform.rotation);
            Destroy(gameObject);
            Destroy(collision.gameObject);
        }
    }
}