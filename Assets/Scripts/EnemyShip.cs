﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShip : MonoBehaviour
{
    public GameObject Explosion;

    private GameObject _Target;

    private GameController _GameController;
    public GameObject BulletPrefab;
    public Transform BulletPoint;

    void Start()
    {
        _GameController = GameObject.Find("GameController").GetComponent<GameController>();

        _Target = GameObject.Find("PlayerShip");
        if (_Target == null) {
            return;
        }

        InvokeRepeating("Attack", 1f, 2f);
    }

    void Update()
    {
        transform.position -= new Vector3(0, Time.deltaTime, 0);

        if (transform.position.y < -3) {
            Destroy(gameObject);
        }
    }

    private void Attack() {
        if (_Target == null) {
            return;
        }

        Vector2 myPos = transform.position;
        Vector2 targetPos = _Target.transform.position;
        GameObject bullet = Instantiate(BulletPrefab, BulletPoint.position, BulletPoint.rotation);
        EnemyBullet enemyBullet = bullet.GetComponent<EnemyBullet>();
        if (enemyBullet == null) {
            return;
        }

        Vector2 basePos = targetPos - myPos;
        float rad = Mathf.Atan2(basePos.y, basePos.x);
        enemyBullet.AddPos = new Vector2(
            Mathf.Cos(rad),
            Mathf.Sin(rad)
        );
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag.ToString() == "PlayerBullet") {
            Instantiate(Explosion, transform.position, transform.rotation);
            Destroy(gameObject);
            Destroy(collision.gameObject);
            _GameController.AddScore(100);
        }
    }
}
